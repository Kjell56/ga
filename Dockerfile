

FROM jupyter/minimal-notebook:2343e33dec46
#RUN pip install --upgrade pip && \
#    pip install --no-cache-dir nbgitpuller && \
#    pip install jupyter-lsp
#RUN jupyter labextension install @jupyterlab/git
#RUN jupyter labextension install jupyterlab_voyager
#RUN jupyter labextension install jupyterlab-drawio
#RUN jupyter labextension install @jupyterlab/plotly-extension
#RUN jupyter labextension install jupyterlab_bokeh
#RUN jupyter labextension install @krassowski/jupyterlab-lsp
#RUN pip install python-language-server[all]

#USER root

### BASICS ###
# Technical Environment Variables
#ENV \
#    SHELL="/bin/bash" \
#    HOME="/root"  \
#    # Nobteook server user: https://github.com/jupyter/docker-stacks/blob/master/base-notebook/Dockerfile#L33
#    NB_USER="root" \
#    USER_GID=0 \
#    XDG_CACHE_HOME="/root/.cache/" \
#    XDG_RUNTIME_DIR="/tmp" \
#    DISPLAY=":1" \
#    TERM="xterm" \
#    DEBIAN_FRONTEND="noninteractive" \
#    RESOURCES_PATH="/resources" \
#    SSL_RESOURCES_PATH="/resources/ssl" \
#    WORKSPACE_HOME="/workspace"

#WORKDIR $HOME

## Make folders
#RUN \
#    mkdir $RESOURCES_PATH && chmod a+rwx $RESOURCES_PATH && \
#    mkdir $WORKSPACE_HOME && chmod a+rwx $WORKSPACE_HOME && \
#    mkdir $SSL_RESOURCES_PATH && chmod a+rwx $SSL_RESOURCES_PATH
#
### VS Code Server: https://github.com/codercom/code-server
#COPY resources/tools/vs-code-server.sh $RESOURCES_PATH/tools/vs-code-server.sh
#RUN \
#    /bin/bash $RESOURCES_PATH/tools/vs-code-server.sh --install && \

#RUN conda install -c r rstudio
#RUN pip install jupyter-server-proxy
#RUN pip install jupyter-rsession-proxy
#RUN pip install jupyter-shiny-proxy


#RUN npm cache clean --force && \
RUN rm -rf $CONDA_DIR/share/jupyter/lab/staging

ENV PYTHONPATH /etc/jupyter
ADD data_science_notebook/__init__.py /etc/jupyter/__init__.py
ADD data_science_notebook/handlers.py /etc/jupyter/handlers.py
ADD data_science_notebook/jupyter_notebook_config.py /etc/jupyter/jupyter_notebook_config.py

USER $NB_USER