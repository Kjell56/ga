from tornado import web
from notebook.base.handlers import IPythonHandler


class ForbidFilesHandler(IPythonHandler):
    @web.authenticated
    def head(self, path):
        self.log.info("HEAD: File download forbidden.")
        raise web.HTTPError(403)

    @web.authenticated
    def get(self, path, include_body=True):
        self.log.info("GET: File download forbidden.")
        raise web.HTTPError(403)

    @web.authenticated
    def put(self, path, include_body=True):
        self.log.info("PUT: File upload forbidden.")
        raise web.HTTPError(403)

    @web.authenticated
    def post(self, path, include_body=True):
        self.log.info("POST: File upload forbidden.")
        raise web.HTTPError(403)
